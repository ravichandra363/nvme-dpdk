#include "main.h"

#define MAX_SRV_BURST 2

extern LL nb_tx[100];
extern LL nb_rx[100];
extern LL nb_fails[100];
	
void run_server(struct rte_mempool **l2fwd_pktmbuf_pool)
{
    int i;
    struct rte_mbuf *rx_pkts_burst[MAX_SRV_BURST];

    int lcore_id = rte_lcore_id();
    int socket_id = rte_lcore_to_socket_id(lcore_id);

    int queue_id = get_lcore_rank(lcore_id, socket_id);

    printf("Server on lcore %d. Queue Id = %d\n", lcore_id, queue_id);

    struct ether_hdr *eth_hdr;
    struct ipv4_hdr *ip_hdr;
    void *src_mac_ptr, *dst_mac_ptr;

    // sizeof(ether_hdr) + sizeof(ipv4_hdr) is 34 --> 36 for 4 byte alignment
    int hdr_size = 36;
    uint64_t rss_seed = 0xdeadbeef;

    FILE *fp = fopen("/test/largefile/file.txt", "r");
    if(!fp)
    {
        printf("fopen error\n");
        exit(0);
    }

#if 0
    int fd = open("file.txt", O_RDWR);
    if(!fd)
    {
        printf("Error in file open\n");
        exit(0);
    }
    struct stat statbuf;
    if (fstat (fd,&statbuf) < 0)
    {
        printf("statbuf error\n");
        exit(0);
    }
    printf("Size of file.txt = %d\n", (int)statbuf.st_size);

    char *fh = (char *)mmap(NULL, statbuf.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(fh == MAP_FAILED)
    {
        printf("File mapping failed\n");
        exit(0);
    }
#endif

    alarm(10);
    int port_id = 0;
    while (1) 
    {
        int nb_rx_new = 0, tries = 0;
		
        // Lcores *cannot* wait for a particular number of packets from a port. If we do this,
        // the port mysteriously runs out of RX descriptors.
        while(nb_rx_new < MAX_SRV_BURST && tries < 5) 
        {
            nb_rx_new += rte_eth_rx_burst(port_id, queue_id, &rx_pkts_burst[nb_rx_new], 
                MAX_SRV_BURST - nb_rx_new);
            tries++;
        }
		
        if(nb_rx_new == 0) 
        {
            //port_index = (port_index + 1) % num_active_ports;
            continue;
        }
	
        nb_rx[lcore_id] += nb_rx_new;
		
        for(i = 0; i < nb_rx_new; i++) 
        {
            // Boilerplate for TX pkt
            if(i != nb_rx_new - 1) 
                rte_prefetch0(rte_pktmbuf_mtod(rx_pkts_burst[i + 1], void *));

            eth_hdr = rte_pktmbuf_mtod(rx_pkts_burst[i], struct ether_hdr *);
            ip_hdr = (struct ipv4_hdr *) ((char *) eth_hdr + sizeof(struct ether_hdr));
            
            src_mac_ptr = &eth_hdr->s_addr.addr_bytes[0];
            dst_mac_ptr = &eth_hdr->d_addr.addr_bytes[0];
            swap_mac(src_mac_ptr, dst_mac_ptr);

            eth_hdr->ether_type = htons(ETHER_TYPE_IPv4);
    
            // These 3 fields of ip_hdr are required for RSS
            ip_hdr->src_addr = fastrand(&rss_seed);
            ip_hdr->dst_addr = fastrand(&rss_seed);
            ip_hdr->version_ihl = 0x40 | 0x05;

            rx_pkts_burst[i]->pkt.nb_segs = 1;
            rx_pkts_burst[i]->pkt.pkt_len = CUR_PKT_LEN;
            rx_pkts_burst[i]->pkt.data_len = CUR_PKT_LEN;
            int *offset = (int *)(rte_pktmbuf_mtod(rx_pkts_burst[i], char *) + hdr_size);
            char *resp = (char *)(rte_pktmbuf_mtod(rx_pkts_burst[i], char *) + hdr_size + sizeof(int));

            fseek(fp, offset[0], SEEK_SET);
            fread(resp, 1, FILE_DATA_LEN, fp);
            //memcpy(resp, &fh[offset[0]], FILE_DATA_LEN);
        }
    
        int nb_tx_new = rte_eth_tx_burst(port_id, queue_id, rx_pkts_burst, nb_rx_new);
        nb_tx[lcore_id] += nb_tx_new;

        // Free unsent packets
        for(i = nb_tx_new; i < nb_rx_new; i ++) 
            rte_pktmbuf_free(rx_pkts_burst[i]);

        //port_index = (port_index + 1) % num_active_ports;
        //usleep(20000);
    }
}
