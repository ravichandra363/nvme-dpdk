#include "main.h"
#define MAX_CLT_TX_BURST 2
#define MAX_CLT_RX_BURST 2

extern LL nb_tx[100];
extern LL nb_rx[100];
extern LL nb_fails[100];

char fnames[10][6] = {"file0", "file1", "file3", "file4", "file5", 
                      "file6", "file7", "file8", "file9", "filea"};

void run_client(int client_id, struct rte_mempool **l2fwd_pktmbuf_pool)
{
    // Node - 2
    LL src_mac_arr[2][4] = {{0xfcf2289f36a0, 0xfcf2289f36a0, 0xfcf2289f36a0, 0xfcf2289f36a0},
                                      {0xfcf2289f36a0, 0xfcf2289f36a0, 0xfcf2289f36a0, 0xfcf2289f36a0}};

    // Node - 3
    //LL src_mac_arr[2][4] = {{0x78fa289f36a0, 0x78fa289f36a0, 0x78fa289f36a0, 0x78fa289f36a0},
    //                                     {0x78fa289f36a0, 0x78fa289f36a0, 0x78fa289f36a0, 0x78fa289f36a0}};
    LL dst_mac_arr[2][4] = {{0x205d2a9f36a0, 0x205d2a9f36a0, 0x205d2a9f36a0, 0x205d2a9f36a0},
                            {0x205d2a9f36a0, 0x205d2a9f36a0, 0x205d2a9f36a0, 0x205d2a9f36a0}};

    // Even cores take xge0,1. Odd cores take xge2, xge3
    int lcore_to_port[12] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    int i;

    struct rte_mbuf *rx_pkts_burst[MAX_CLT_RX_BURST], *tx_pkts_burst[MAX_CLT_TX_BURST];

    int lcore_id = rte_lcore_id();

    int port_id = lcore_to_port[lcore_id];
    if(!ISSET(XIA_R0_PORT_MASK, port_id)) 
    {
        red_printf("Lcore %d uses disabled port (port %d). Exiting.\n", lcore_id, port_id);
        exit(-1);
    }

    int socket_id = rte_lcore_to_socket_id(lcore_id);
    int queue_id = get_lcore_rank(lcore_id, socket_id);
    red_printf("Client %d: lcore: %d, port: %d, queue: %d\n", client_id, lcore_id, port_id, queue_id);

    struct ether_hdr *eth_hdr;
    struct ipv4_hdr *ip_hdr;
    uint8_t *src_mac_ptr, *dst_mac_ptr;

    uint64_t rss_seed = 0xdeadbeef;

    // sizeof(ether_hdr) + sizeof(ipv4_hdr) is 34 --> 36 for 4 byte alignment
    int hdr_size = 36;

    int fd = open("file.txt", O_RDWR);
    if(!fd)
    {
        printf("Error in file open\n");
        exit(0);
    }
    struct stat statbuf;
    if (fstat (fd,&statbuf) < 0)
    {
        printf("statbuf error\n");
        exit(0);
    }
    printf("Size of file.txt = %d\n", (int)statbuf.st_size);

    char *fh = (char *)mmap(NULL, statbuf.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(fh == MAP_FAILED)
    {
        printf("File mapping failed\n");
        exit(0);
    }

    alarm(10);

    while (1) 
    {
        for(i = 0; i < MAX_CLT_TX_BURST; i ++) 
        {
            tx_pkts_burst[i] = rte_pktmbuf_alloc(l2fwd_pktmbuf_pool[lcore_id]);
            CPE(tx_pkts_burst[i] == NULL, "tx_alloc failed\n");
            
            eth_hdr = rte_pktmbuf_mtod(tx_pkts_burst[i], struct ether_hdr *);
            ip_hdr = (struct ipv4_hdr *) ((char *) eth_hdr + sizeof(struct ether_hdr));
        
            src_mac_ptr = &eth_hdr->s_addr.addr_bytes[0];

            if((fastrand(&rss_seed) & 0xff) == 0) 
                set_mac(src_mac_ptr, src_mac_arr[client_id][port_id]);
            else 
                set_mac(src_mac_ptr, 0xdeadbeef);

            set_mac(src_mac_ptr, src_mac_arr[client_id][port_id]);
            dst_mac_ptr = &eth_hdr->d_addr.addr_bytes[0];
            set_mac(dst_mac_ptr, dst_mac_arr[client_id][port_id]);

            eth_hdr->ether_type = htons(ETHER_TYPE_IPv4);
    
            // These 3 fields of ip_hdr are required for RSS
            ip_hdr->src_addr = fastrand(&rss_seed);
            ip_hdr->dst_addr = fastrand(&rss_seed);
            ip_hdr->version_ihl = 0x40 | 0x05;

            tx_pkts_burst[i]->pkt.nb_segs = 1;
            tx_pkts_burst[i]->pkt.pkt_len = CUR_PKT_LEN;
            tx_pkts_burst[i]->pkt.data_len = CUR_PKT_LEN;

            int *offset = (int *)(rte_pktmbuf_mtod(tx_pkts_burst[i], char *) + hdr_size);
            offset[0] = rand() % 68719470000;
        }

        int nb_tx_new = rte_eth_tx_burst(port_id, queue_id, tx_pkts_burst, MAX_CLT_TX_BURST);
        nb_tx[lcore_id] += nb_tx_new;
        
        for(i = nb_tx_new; i < MAX_CLT_TX_BURST; i++) 
            rte_pktmbuf_free(tx_pkts_burst[i]);

        micro_sleep(2, C_FAC);

        // RX drain
        while(1) 
        {
            int nb_rx_new = rte_eth_rx_burst(port_id, queue_id, rx_pkts_burst, MAX_CLT_RX_BURST);
            if(nb_rx_new == 0) 
                break;
        
            nb_rx[lcore_id] += nb_rx_new;             
            for(i = 0; i < nb_rx_new; i ++) 
            {
#if 0
                int *offset = (int *)(rte_pktmbuf_mtod(rx_pkts_burst[i], char *) + hdr_size);
                char *resp = (char *)(rte_pktmbuf_mtod(rx_pkts_burst[i], char *) + hdr_size + sizeof(int));
                if(memcmp(resp, &fh[offset[0]], FILE_DATA_LEN) != 0)
                    printf("faulty response from server\n");
#endif
                rte_pktmbuf_free(rx_pkts_burst[i]);
            }
        }
    }
}
