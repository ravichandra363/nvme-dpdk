#include "main.h"
int is_client = -1, client_id;

LL nb_tx[100];
LL nb_rx[100];
LL nb_fails[100];

void sigalarm_handler_client(int signum);
void sigalarm_handler_server(int signum);

int num_lcores = 0;
int coremask = 0;
int count;

void sigalarm_handler_client(int signum)
{
    signal(SIGALRM, SIG_IGN);
    printf("Packets Received: %lld. Packets sent: %lld\n", nb_rx[0], nb_tx[0]);
    nb_tx[0] = 0; nb_rx[0] = 0;
    signal(SIGALRM, sigalarm_handler_client);
    alarm(10);

    return;
}

void sigalarm_handler_server(int signum)
{
    int i = 0;
    LL tot_sent = 0, tot_rec = 0;
    signal(SIGALRM, SIG_IGN);
    count++;

    for(i = 0; i < 32; i++)
    {
        if(!ISSET(coremask, i)) 
            continue;

        tot_sent += nb_tx[i];
        tot_rec += nb_rx[i];
        //printf("Lcore %d: Packets Received: %lld. Packets sent: %lld\n",
        //    i, nb_rx[i], nb_tx[i]);
        nb_tx[i] = 0; nb_rx[i] = 0; 
    }

    printf("Set %d ---> Total received: %lld. Total Sent: %lld\n", count, tot_rec, tot_sent);
    printf("--------------------------------------------------------------\n");
    signal(SIGALRM, sigalarm_handler_server);
    alarm(10);

    return;
}

// Disable all offload features
static const struct rte_eth_conf port_conf = {
	.rxmode = {
		.split_hdr_size = 0,
		.header_split   = 0,
		.hw_ip_checksum = 0,
		.hw_vlan_filter = 0,
		.jumbo_frame    = 0,
		.hw_strip_crc   = 0,
		.mq_mode = ETH_MQ_RX_RSS,
	},
    .rx_adv_conf = {
        .rss_conf = {
            .rss_key = NULL,
			.rss_hf = ETH_RSS_IPV4,
		},
	},
};

static const struct rte_eth_rxconf rx_conf = {
	.rx_thresh = {
		.pthresh = RX_PTHRESH,
		.hthresh = RX_HTHRESH,
		.wthresh = RX_WTHRESH,
	},
	//.rx_free_thresh = DEFAULT_NIC_RX_FREE_THRESH,
	//.rx_drop_en = 0		// No idea what this is..
};

static const struct rte_eth_txconf tx_conf = {
	.tx_thresh = {
		.pthresh = TX_PTHRESH,
		.hthresh = TX_HTHRESH,
		.wthresh = TX_WTHRESH,
	},
	.tx_free_thresh = 0, /* Use PMD default values */
	.tx_rs_thresh = 0, /* Use PMD default values */
        .txq_flags = ETH_TXQ_FLAGS_NOMULTSEGS | ETH_TXQ_FLAGS_NOOFFLOADS,
};

static struct ether_addr l2fwd_ports_eth_addr[RTE_MAX_ETHPORTS];	// MAC addr
struct rte_mempool *l2fwd_pktmbuf_pool[RTE_MAX_LCORE];		// Per lcore mempools

static int
l2fwd_launch_one_lcore(__attribute__((unused)) void *dummy)
{
	if(is_client) {
                printf("launching client..\n");
		run_client(client_id, l2fwd_pktmbuf_pool);
	} else {
                printf("launching server..\n");
		run_server(l2fwd_pktmbuf_pool);
	}
	return 1;
}

int
main(int argc, char **argv)
{
    int ret;
    uint8_t nb_ports;
    uint8_t port_id;
    unsigned lcore_id;

    coremask = strtol(argv[2], NULL, 0);
    if(argc > 5) 
    {        
        // Do this before eal parsing
        is_client = 1;
        client_id = atoi(argv[6]);
        printf("Client, argv[5] = %s, argv[6]=%s\n", argv[5], argv[6]);
    } 
    else 
        is_client = 0;

    if(is_client)
        signal(SIGALRM, sigalarm_handler_client);
    else
        signal(SIGALRM, sigalarm_handler_server);

    ret = rte_eal_init(argc, argv);
    CPE(ret < 0, "Invalid EAL arguments\n");
    CPE(rte_eal_pci_probe() < 0, "Cannot probe PCI\n");

    nb_ports = rte_eth_dev_count();
        printf("Number of ports: %d\n", (int)nb_ports);
    nb_ports = nb_ports > RTE_MAX_ETHPORTS ? RTE_MAX_ETHPORTS : nb_ports;
    CPE(nb_ports == 0, "No Ethernet ports - bye\n");

    printf("\n\n");
    // Create a mempool for each enabled lcore
    for(lcore_id = 0; lcore_id < RTE_MAX_LCORE; lcore_id ++) {
        if(rte_lcore_is_enabled(lcore_id)) {
                        num_lcores++;
            char pool_name[20];
            sprintf(pool_name, "pool_%d", lcore_id);
            red_printf("Lcore %d is enabled. Creating mempool for it on socket %d\n", 
                //lcore_id, (int)rte_socket_id());
                lcore_id, (int)rte_lcore_to_socket_id(lcore_id));
            l2fwd_pktmbuf_pool[lcore_id] = mempool_init(pool_name, rte_lcore_to_socket_id(lcore_id));
            CPE(l2fwd_pktmbuf_pool[lcore_id] == NULL, "Cannot init mbuf pool\n");
        }
    }

    /* Initialise each port */
    int portmask = is_client == 1 ? XIA_R0_PORT_MASK : XIA_R2_PORT_MASK;
    red_printf("\nInitializing ports\n");

    for (port_id = 0; port_id < nb_ports; port_id++) 
    {
        if (!ISSET(portmask, port_id)) 
            continue;

        printf("portid %d is being configured\n", (int)port_id);
        int my_socket_id = rte_eth_dev_socket_id(port_id);
        if(my_socket_id < 0) 
        {
            red_printf("Socket id is less than 0..exiting\n");
            exit(-1);
        }

            int num_queues = count_active_lcores_on_socket(my_socket_id);
            printf("Number of queues/lcores on socket %d: %d\n", my_socket_id, num_queues);

            printf("Initializing port %u on socket %d with %d queues \n", 
                (unsigned) port_id, my_socket_id, num_queues);

            ret = rte_eth_dev_configure(port_id, num_queues, num_queues, &port_conf);
            CPE2(ret < 0, "Cannot configure device: %d, %u\n", ret, (unsigned) port_id);

            int queue_id = 0;
            for(queue_id = 0; queue_id < num_queues; queue_id++) 
            {
                int my_lcore_id = get_lcore_ranked_n(queue_id, my_socket_id);
    
                if(rte_lcore_is_enabled(my_lcore_id) == 0) 
                {
                    red_printf("\tQueue %d on port %d wants disabled lcore %d! Exiting!\n", 
                        queue_id, port_id, my_lcore_id);
                    exit(-1);
                }

                struct rte_mempool *mp = l2fwd_pktmbuf_pool[my_lcore_id];
                printf("\tSetting up queue %d using lcore %d's mempool\n", queue_id, my_lcore_id);
                fflush(stdout);
                ret = rte_eth_rx_queue_setup(port_id, queue_id, NUM_RX_DESC, my_socket_id, &rx_conf, mp);
                CPE2(ret < 0, "rte_eth_rx_queue_setup: %d, %u\n", ret, (unsigned) port_id);
    
                fflush(stdout);
                ret = rte_eth_tx_queue_setup(port_id, queue_id, NUM_TX_DESC, my_socket_id, &tx_conf);
                CPE2(ret < 0, "rte_eth_tx_queue_setup: %d, %u\n", ret, (unsigned) port_id);
            }

        // Print device mac address and start it
        rte_eth_macaddr_get(port_id, &l2fwd_ports_eth_addr[port_id]);
        print_mac(port_id, l2fwd_ports_eth_addr[port_id]);

        ret = rte_eth_dev_start(port_id);
        CPE2(ret < 0, "rte_eth_dev_start: %d, %u\n", ret, (unsigned) port_id);
                rte_eth_promiscuous_enable(port_id);

    }

    check_all_ports_link_status(nb_ports, portmask);

    /* launch per-lcore init on every lcore */
    rte_eal_mp_remote_launch(l2fwd_launch_one_lcore, NULL, CALL_MASTER);
    RTE_LCORE_FOREACH_SLAVE(lcore_id) 
    {
        int ret;
        if ((ret = rte_eal_wait_lcore(lcore_id)) < 0) 
        {
            printf("rte_eal_wait_lcore for lcoreid=%d returned %d\n", lcore_id, ret);
            return -1;
        }
    }
    return 0;
}

